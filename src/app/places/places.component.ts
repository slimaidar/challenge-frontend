import { Component, OnInit } from '@angular/core';
import {PlacesService} from '../services/places.service';

@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styleUrls: ['./places.component.css']
})
export class PlacesComponent implements OnInit {

  places: any;
  constructor(private placesService: PlacesService) { }

  ngOnInit() {
    const plSer = this.placesService;
    plSer.getPlaces()
      .subscribe(res => {
        this.places = res['results'];
        console.log(this.places);
      }, err => {
        console.log(err);
      });
  }

  likePlace(place) {
    this.placesService.likePlace(place);
  }

}
