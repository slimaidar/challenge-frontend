import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../services/authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  userData = {
    email: '',
    password: '',
  };
  password2 = '';
  errors: any;
  error = false;

  constructor(private authService: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.password2 === this.userData.password) {
      this.authService.register(this.userData)
        .subscribe(res => {
          this.router.navigateByUrl('/login');
        }, err => {
          console.log(err.error);
          this.error = true;
          this.errors = <Object>err.error;
        });
    }
  }
}
