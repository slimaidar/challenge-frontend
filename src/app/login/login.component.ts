import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../services/authentication.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userData = {
    email: '',
    password: ''
  };
  error = false;
  errorMsg = '';

  constructor(private authService: AuthenticationService, private router:Router) { }

  ngOnInit() {
  }

  onSubmit() {
    this.authService.login(this.userData)
      .subscribe(res => {
        localStorage.setItem('token', res['token']);
        this.router.navigateByUrl('/');
      }, err => {
        this.errorMsg = err.error.non_field_errors[0];
        this.error = true;
      });
  }

}
