import { Component, OnInit } from '@angular/core';
import {FavoritesService} from '../services/favorites.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css']
})
export class FavoritesComponent implements OnInit {

  places: any;

  constructor(private favService: FavoritesService) { }

  ngOnInit() {
    this.favService.getFavoritePlaces().subscribe(res => this.places = res);
  }

  dislikePlace(id) {
    this.favService.dislikePlace(id);
    let newPlaces = [];
    for (let place of this.places) {
      if ( id !== place.id ) {
        newPlaces.push(place);
      }
    }
    this.places = newPlaces;
  }

}
