import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/internal/Observable';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class AuthInterceptors implements HttpInterceptor {
  constructor(private authService: AuthenticationService) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({
       setHeaders: {
         Authorization: 'Bearer ' + this.authService.getToken()
     }} );
    return next.handle(req);
  }

}
