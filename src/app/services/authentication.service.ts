import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  public login(user) {
    return this.http.post('http://127.0.0.1:8000/api/user/login/', user);
  }

  public register(user) {
    return this.http.post('http://127.0.0.1:8000/api/user/create/', user);
  }

  public isAuth() {
    return !!localStorage.getItem('token');
  }

  public getToken() {
    return localStorage.getItem('token');
  }

  public logOut() {
    localStorage.removeItem('token');
  }
}
