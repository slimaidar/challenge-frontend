import { Injectable } from '@angular/core';
import { LocalStorage} from '@ngx-pwa/local-storage';
import {PlacesService} from './places.service';

@Injectable({
  providedIn: 'root'
})
export class FavoritesService {

  constructor(protected localStorage: LocalStorage) { }

  public getFavoritePlaces() {
    return this.localStorage.getItem('places');
  }

  public dislikePlace = (id) => {

    this.getFavoritePlaces()
      .subscribe(res => {
        const places = [];
        for (let i = 0; i < res.length ; i++ ) {
          if (res[i].id !== id) {
            places.push(res[i]);
          }
        }
        this.localStorage.clear().subscribe(() => {});;
        this.localStorage.setItem('places', places).subscribe(() => {});
      });
  }
}
