import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { LocalStorage} from '@ngx-pwa/local-storage';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {

  lat: number;
  long: number;

  constructor(private http: HttpClient, protected localStorage: LocalStorage) {this.getCordinates(); }

  getPlaces() {
    const url = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=${this.lat},${this.long}&rankby=distance&key=AIzaSyCT6LNjPviTG59jkEbHNm8yvxqkKGidfcw`;
    return this.http.get(url);
  }

  getCordinates() {
    if (!navigator.geolocation) {
      alert('geo location not supported');
    } else {
      navigator.geolocation.getCurrentPosition((position) => {
        this.lat = position.coords.latitude;
        this.long = position.coords.longitude;
      });
    }
  }

  likePlace(place) {
    this.localStorage.getItem('places')
        .subscribe((res) => {
          let places = [];
          if (res !== null) {
            places = res;
          }
          if (!this.isPlaceInArray(place, places)) {
            console.log(this.isPlaceInArray(place, places));
            places.push(place);
            console.log('added');
            this.localStorage.setItem('places', places).subscribe(() => {});
          }
        });
  }

  isPlaceInArray(place, list): boolean {
    if (list === null || list === undefined || list.length === 0) {
      return false;
    } else {
      for (let i = 0; i < list.length; i++) {
        if (list[i].id === place.id) {
          return true;
        }
      }
    }
    return false;
    }
}
